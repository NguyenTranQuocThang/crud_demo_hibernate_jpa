package com.Hibernate.Hibernate.Entity.Request;

import javax.persistence.*;


public class StudentRequest {
    String name;
    int classId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classObj) {
        this.classId = classObj;
    }

    public StudentRequest(String name, int classId) {
        this.name = name;
        this.classId = classId;
    }
}
