package com.Hibernate.Hibernate.Entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "classes")
public class Classes {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    int id;
    String name;
    @OneToMany(mappedBy = "classId")
    Set<Student> studentSet = new HashSet<>();
    public int getId() {
        return id;
    }

    public Classes() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Classes(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Set<Student> getStudentSet() {
        return studentSet;
    }

    public void setStudentSet(Set<Student> studentSet) {
        this.studentSet = studentSet;
    }
}
