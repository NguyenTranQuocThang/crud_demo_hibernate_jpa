package com.Hibernate.Hibernate.Reposity;

import com.Hibernate.Hibernate.Entity.Student;


import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student,Integer>   {
    List<Student> findByClassId(Integer classId, Pageable pageable);
}
