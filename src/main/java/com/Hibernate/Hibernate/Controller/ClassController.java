package com.Hibernate.Hibernate.Controller;

import com.Hibernate.Hibernate.Entity.Classes;
import com.Hibernate.Hibernate.Entity.Request.ClassRequest;
import com.Hibernate.Hibernate.Service.ClassesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/api/v1/class")
public class ClassController {
    @Autowired
    ClassesService classService;

    @GetMapping("")
    public ResponseEntity<List<Classes>> getAllClass(){
        return new ResponseEntity<>(classService.getAllClass(), HttpStatus.OK);
    }
    @PostMapping("")
    public ResponseEntity<List<Classes>> createClass(@RequestBody List<ClassRequest> classRequests){
        return new ResponseEntity<>(classService.createClass(classRequests),HttpStatus.OK);
    }
}
