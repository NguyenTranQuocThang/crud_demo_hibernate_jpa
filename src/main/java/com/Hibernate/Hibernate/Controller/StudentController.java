package com.Hibernate.Hibernate.Controller;

import com.Hibernate.Hibernate.Entity.Dto.StudentDto;
import com.Hibernate.Hibernate.Entity.Student;
import com.Hibernate.Hibernate.Entity.Request.StudentRequest;
import com.Hibernate.Hibernate.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import java.util.List;


@Controller
@RequestMapping("/api/v1/Student")
public class StudentController {
    @Autowired
    StudentService studentService;
    private static final String pageInfor = "0";
    private static final String sizeInfor = "1";

    @GetMapping("")
    public ResponseEntity<List<StudentDto>> getAll(@RequestParam(name = "idSearch",required = false) Integer id,
                                                   @RequestParam(name = "page",defaultValue = pageInfor) Integer page,
                                                   @RequestParam(name = "size",defaultValue = sizeInfor) Integer size) {
            return new ResponseEntity<>(studentService.getAll(id,page,size), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<List<StudentDto>> createStudent(@RequestBody List<StudentRequest> student) {
        return new ResponseEntity<>(studentService.createStudent(student), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Student> updateStudent(@PathVariable(name = "id") Integer id, @RequestBody StudentRequest studentRequest) {
        return new ResponseEntity<>(studentService.updateStudentById(id,studentRequest),HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteStudent(@PathVariable(name = "id") Integer id ){
        return new ResponseEntity<>(studentService.deleteStudentById(id),HttpStatus.OK);
    }
}
