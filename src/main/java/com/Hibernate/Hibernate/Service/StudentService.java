package com.Hibernate.Hibernate.Service;

import com.Hibernate.Hibernate.Entity.Dto.StudentDto;
import com.Hibernate.Hibernate.Entity.Student;
import com.Hibernate.Hibernate.Entity.Request.StudentRequest;

import java.util.List;
import java.util.Optional;


public interface StudentService {
    List<StudentDto> getAll(Integer classId,Integer page, Integer size);
    List<StudentDto> createStudent(List<StudentRequest> studentRequest);
    Student updateStudentById(Integer id,StudentRequest studentRequest);
    Boolean deleteStudentById(Integer id);
}
