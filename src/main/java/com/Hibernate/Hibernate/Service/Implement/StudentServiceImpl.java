package com.Hibernate.Hibernate.Service.Implement;

import com.Hibernate.Hibernate.Entity.Dto.StudentDto;
import com.Hibernate.Hibernate.Entity.Student;
import com.Hibernate.Hibernate.Entity.Request.StudentRequest;
import com.Hibernate.Hibernate.Reposity.StudentRepository;
import com.Hibernate.Hibernate.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    StudentRepository studentRepository;

    public List<StudentDto> changeToListStudentDto(List<Student> studentList) {
        List<StudentDto> studentDto = new ArrayList<>();
        for (Student s : studentList
        ) {
            StudentDto dto = new StudentDto();
            dto.setName(s.getName());
            studentDto.add(dto);
        }
        return studentDto;
    }

    public List<Student> changeToListStudent(List<StudentRequest> studentRequests) {
        List<Student> studentList = new ArrayList<>();
        for (StudentRequest s : studentRequests
        ) {
            Student student = new Student();
            student.setName(s.getName());
            student.setClassId(s.getClassId());
            studentList.add(student);
        }
        return studentList;
    }

    @Override
    public List<StudentDto> getAll(Integer classId,Integer page,Integer size) {
        List<StudentDto> studentDtoList;
        if (null == classId) {
            Page<Student> studentPage = studentRepository.findAll(PageRequest.of(page,size));
            studentDtoList = changeToListStudentDto(studentPage.getContent());
        } else {
            studentDtoList = changeToListStudentDto(studentRepository.findByClassId(classId, PageRequest.of(page,size)));
        }
        return studentDtoList;
    }

    @Override
    public List<StudentDto> createStudent(List<StudentRequest> studentRequest) {
        if (null == studentRequest) {
            return null;
        } else {
            List<Student> studentList = changeToListStudent(studentRequest);
            List<StudentDto> studentDtoList = changeToListStudentDto(studentRepository.saveAll(studentList));
            return studentDtoList;
        }
    }

    @Override
    public Student updateStudentById(Integer id, StudentRequest studentRequest) {
        if(id != null && studentRequest != null){
            Optional<Student> student = studentRepository.findById(id);
            if (student.isPresent()) {
                return studentRepository.save(new Student(id, studentRequest.getName(), studentRequest.getClassId()));
            } else {
                return null;
            }
        }else{
            return null;
        }
    }

    public Boolean deleteStudentById(Integer id) {
        if(id == null){
            return null;
        }else{
            Optional<Student> student = studentRepository.findById(id);
            if (student.isPresent()) {
                studentRepository.deleteById(id);
                return true;
            } else {
                return false;
            }
        }
    }
}
