package com.Hibernate.Hibernate.Service.Implement;

import com.Hibernate.Hibernate.Entity.Classes;
import com.Hibernate.Hibernate.Entity.Request.ClassRequest;
import com.Hibernate.Hibernate.Reposity.ClassesRepository;
import com.Hibernate.Hibernate.Service.ClassesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClassesServiceImpl implements ClassesService {
    @Autowired
    ClassesRepository classRepository;

    List<Classes> changetoClass(List<ClassRequest> classRequests){
        List<Classes> classList = new ArrayList<>();
        for (ClassRequest c:classRequests
             ) {
            Classes cl = new Classes();
            cl.setName(c.getName());
            classList.add(cl);
        }
        return classList;
    }

    @Override
    public List<Classes> getAllClass() {
        List<Classes> classList = classRepository.findAll();
        return classList;
    }

    @Override
    public List<Classes> createClass(List<ClassRequest> classRequests) {
        if(classRequests == null){
            return null;
        }else{
            List<Classes> classList = changetoClass(classRequests);
            return classRepository.saveAll(classList);
        }
    }
}
