package com.Hibernate.Hibernate.Service;

import com.Hibernate.Hibernate.Entity.Classes;
import com.Hibernate.Hibernate.Entity.Request.ClassRequest;

import java.util.List;

public interface ClassesService {
    List<Classes> getAllClass();
    List<Classes> createClass(List<ClassRequest> classRequests);
}
